---
sidebar_position: 2
title: Quick Links
---

## Portals
- Pontus-X Gaia-X Web3 Ecosystem Portal: https://portal.minimal-gaia-x.eu/
- EuProGigant Portal: https://portal.euprogigant.io/
- Gaia-X 4 Future Mobility moveID Portal: https://portal.moveid.eu/
- State Library of Berlin Portal: https://sbb.portal.minimal-gaia-x.eu/    
- Future Mobility Data Marketplace: https://marketplace.future-mobility-alliance.org/
- PEAQ Portal: https://gaia-x.portal.peaq.network/
- University of Lleida Open Science Portal: https://udl.portal.minimal-gaia-x.eu/

## GEN-X Services
- GEN-X Testnet Explorer: https://logging.genx.minimal-gaia-x.eu/ 
- GEN-X Faucet: https://faucet.genx.minimal-gaia-x.eu/