---
sidebar_position: 2
title: Current Participants
---


## List of Participants (WIP)

| Participant                         | Public Address                             | Status        | Gaia-X Participant Self-Description                                  |
| ----------------------------------- | ------------------------------------------ | ------------- | -------------------------------------------------------------------- |
| deltaDAO AG                         | 0x4C84a36fCDb7Bc750294A7f3B5ad5CA8F74C4A52 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xa76Fa6837A6ffc9F123F2193717A5965c68B0cbA | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x2859d961a6dBa6e7d30b2d383Af468edb4E7F4f6 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xaBaf56FC1bB6b4FF9fA4378C3C8723d2B2444324 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0x68C24FA5b2319C81b34f248d1f928601D2E5246B | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xa76Fa6837A6ffc9F123F2193717A5965c68B0cbA | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xFaeb57c16D5E9A9f06c8c5dB12796f5a432Eb7d6 | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| deltaDAO AG                         | 0xb501FDaad0F0863C4c72f7EB9Abc23965DCa973d | Federator     | https://delta-dao.com/.well-known/participantdeltadao.json           |
| PTW - TU Darmstadt                  | 0x4A806a4851472F7cFd579d3FF5465F03c3c2B5d4 | Federator     | https://ptw.tu-darmstadt.euprogigant.io/sd/participant.json          |
| IONOS Cloud                         | 0x81336c245712DbF0E971de5463173bCaA9826d84 | Federator     | https://delta-dao.com/.well-known/participantIONOS.json              |
| Exoscale                            |                                            | Federator     | https://a1.digital.euprogigant.io/sd/participant.json                |
| Wobcom                              | 0x9Dc6aDA184fc98012D74F1C4f3f223183A4745D4 | Federator     | https://delta-dao.com/.well-known/participantWobcom.json             |
| Arsys                               | 0x0337b320DEfAddd9aDbC518f8A9cee30b606d15b | Federator     | https://arlabdevelopments.com/.well-known/ArsysParticipant.json      |
| State Library of Berlin             | 0xF20113edd04d98A64AD2A003B836677E1c9aACAD | Federator     | https://delta-dao.com/.well-known/participantStateLibraryBerlin.json |
| University of Lleida                | 0x62078F05Eb4450272D7E492F3660835826906822 | Federator     | https://delta-dao.com/.well-known/participantUniversitydeLleida.json |
| Materna SE                          | 0xf596D17C4a3A5c92c4721627B9e5E5064651BF46 | Participant   | https://delta-dao.com/.well-known/participantMaterna.json            |
| Airbus                              | 0x3CEA8fBCbD1c745E081fD3937C18eE0b6Cc3f1b1 | Participant   | https://delta-dao.com/.well-known/participantAirbus.json             |
| OHB                                 | 0xF8dB4a6d529a14e3E029e2b8A9279f408909Fa20 | Participant   | https://delta-dao.com/.well-known/participantOHBdigital.json         |
| T-Systems Multimedia Solutions GmbH | 0x48535044200dAE3FD4f5b5C3f9b077fa5c230Ef3 | Participant   | https://delta-dao.com/.well-known/participantTsystemsMMS.json        |
| IKS – Gesellschaft für Informations- und Kommunikationssysteme mbH | 0x212c355c3ce41a272606da61F661dDd2b7F8a4B1 | Participant   | https://delta-dao.com/.well-known/participantIKS.json |
| msg David GmbH                      | 0x44C34FbBB727bDC648E65feCfF3FB9D4c85f1fe4 | Participant   | https://delta-dao.com/.well-known/participantMsgDavid.json           |
| Detecon                             | 0x8fBF860883BB71D691053A4363030Dc1c65f7017 | Participant   | https://delta-dao.com/.well-known/participantDetecon.json            |
| Perpetuum Progress                  | 0x2ee3c4F19f90237B7C45cfAD6B5dC4b5840563Ec | Participant   | https://delta-dao.com/.well-known/participantPerpetuumProgress.json  |
| SINTEF                              | 0x7DF1674a1e3449778eEB324652d3FF3Cb5046753 | Participant   |                                                                      |
| Peaq                                | 0xe3Df4851c094f5F6F1AC9AbfA4FC2075276195Ec | Participant   | https://delta-dao.com/.well-known/participantPeaq.json               |
| Datarella                           | 0x0aec046a558F13Ff18aAEc5E6f76084185358cdf | Participant   | https://delta-dao.com/.well-known/participantDatarella.json          |
| Bosch                               | 0x6fE8aD445AD86b3d1325F79955Ef28d6e9cb2258 | Participant   | https://delta-dao.com/.well-known/participantBosch.json              |
| TU Dortmund                         | 0x51Decd187744bCfAD1BAb0A3E71dD68fAC0ba478 | Participant   |                                                                      |
| RWS                                 | 0xa98A6eefbAE870b88a9C7A43f4b50066A01c93b6 | Participant   | https://delta-dao.com/.well-known/participantRijkswaterstaat.json    |
| Craftworks                          | 0x9dfbda23b65efB1c836828D74a96eB8528A60f3C | Participant   |https://craftworks.euprogigant.io/sd/participant.json                 |
| Concircle                           |                                            | Participant   |                                                                      |
| Software AG                         |                                            | Participant   |                                                                      |
| DENSO AUTOMOTIVE Deutschland GmbH   | 0x2b92BF0496B7B41ea2d723325DDE96651795c784 | Participant   |                                                                      |
| Struggle Creative Oy                | 0x895975869261A215813e33568a295F94A3F301ed | Participant   | https://delta-dao.com/.well-known/participantStruggle.json           |
| Sphereon                            |                                            | Participant   |                                                                      |
| Austrian Institute of Technology    | 0xfc739f2F91921eb710878ad2Ca38C147a784C96f | Participant   | https://delta-dao.com/.well-known/participantait.json                                                                     |
| acatech                             | 0xF62bF6371Ee020Cb2164Ac3C338514DBbb93A0D4 | Participant   | https://delta-dao.com/.well-known/participantAcatech.json            |
| Spanish Ministry of Economic Affairs and Digital Transformation| 0x6E1cE3530A12F89cF567788C132454E5dC7D3cCE |Participant| https://delta-dao.com/.well-known/participantspanishministryofeconomicaffairs.json.json               |
| ATOS                                | 0x943CaA8afCAdd2F64a7cE9E53A91d5ea0BEb40c1 | Participant   | https://delta-dao.com/.well-known/participantATOS.json               |
| TU Delft                            | 0x9391291b0Df512B20810183744De8272774b6655 | Participant   | https://delta-dao.com/.well-known/participantTUDelft.json               |
| Bigchain DB                         | 0x61DB12d8b636Cb49ea09eCa58a893dA9480E1F33 | Participant   | https://delta-dao.com/.well-known/participantBigchainDB.json         |
| Perpetuum Progress GmbH             | 0x2ee3c4F19f90237B7C45cfAD6B5dC4b5840563Ec | Participant   | https://delta-dao.com/.well-known/participantPerpetuumProgress.json  |
| Mercedes-Benz Singapore Pte. Ltd. | 0x203C7AA993EED06932FA327a192de9A8370b5Ab4 | Participant   | https://www.delta-dao.com/.well-known/participantMercedesBenzLtd.json  |
| Mercedes-Benz Singapore Pte. Ltd. | 0x4d6240C7Ef355a2E85c13B26A49A35908ce853E5 | Participant   | https://www.delta-dao.com/.well-known/participantMercedesBenzLtd.json  |
| Gaia-X Workshop 01 | 0x6bF77769e84045a9EAC64573e70a5562457C52ad | Participant   | https://www.delta-dao.com/.well-known/01.json  |
| Gaia-X Workshop 02 | 0x17c8D253443F9E7305A2539d7aF177B21aAD3355 | Participant   | https://www.delta-dao.com/.well-known/02.json  |
| Gaia-X Workshop 03 | 0xFDF411B7A23182e7F0a635bdF0d25f0fCb2aAf74 | Participant   | https://www.delta-dao.com/.well-known/03.json  |
| Gaia-X Workshop 04 | 0x3560626F234eD181E807E4e31ded56D9aca1ac58 | Participant   | https://www.delta-dao.com/.well-known/04.json  |
| Gaia-X Workshop 05 | 0xF0926FbE8e60E54aFB4fD296B2698230ab32799b | Participant   | https://www.delta-dao.com/.well-known/05.json  |
| Gaia-X Workshop 06 | 0xD59f8A2B75A9817e77a4b30e105C85F908142D38 | Participant   | https://www.delta-dao.com/.well-known/06.json  |
| Gaia-X Workshop 07 | 0xAA782a260Ad773bca5Ff0535356CB0F7B94Cd254 | Participant   | https://www.delta-dao.com/.well-known/07.json  |
| Gaia-X Workshop 08 | 0x2aC6802160A74677B7cEC1aaD7E41Ec968D57896 | Participant   | https://www.delta-dao.com/.well-known/08.json  |
| Gaia-X Workshop 09 | 0xFd1BEC7E551fAeA6102045D720dD693c4e9C8E06 | Participant   | https://www.delta-dao.com/.well-known/09.json  |
| Gaia-X Workshop 10 | 0xa2199E3f60fC244037Efd5A77714CC05F604F855 | Participant   | https://www.delta-dao.com/.well-known/10.json  |
| Gaia-X Workshop 11 | 0x5101ea56E29f5dc03285809b6157f0588ff255D0 | Participant   | https://www.delta-dao.com/.well-known/11.json  |
| Gaia-X Workshop 12 | 0x8B7f2b75B7F87D3125C8B0eDB85639B441BBcE21 | Participant   | https://www.delta-dao.com/.well-known/12.json  |
| Gaia-X Workshop 13 | 0xb11124Dfa40E44b3283068fd07bf6FdE60caf06A | Participant   | https://www.delta-dao.com/.well-known/13.json  |
| Gaia-X Workshop 14 | 0x632460b14aDd90aD9430e381B4662779cC1ab7a6 | Participant   | https://www.delta-dao.com/.well-known/14.json  |
| Gaia-X Workshop 15 | 0x1f65110b63B6044f1E92543C09231842131798C7 | Participant   | https://www.delta-dao.com/.well-known/15.json  |
| Gaia-X Workshop 16 | 0x7A6246e02B2aA276203469Cfb839a2666520D8b5 | Participant   | https://www.delta-dao.com/.well-known/16.json  |
| Gaia-X Workshop 17 | 0x37e01308d6A0E322dECc457a13E0B2b2086D84B1 | Participant   | https://www.delta-dao.com/.well-known/17.json  |
| Gaia-X Workshop 18 | 0xDFa29AE20eac7f203DdDbe15E1830985e99143B8 | Participant   | https://www.delta-dao.com/.well-known/18.json  |
| Gaia-X Workshop 19 | 0xFfA05d656465568BE83B11bf274c5458AC8401AC | Participant   | https://www.delta-dao.com/.well-known/19.json  |
| Gaia-X Workshop 20 | 0xb500BfE3d89b5D6b0d2b91841c3A3aD568Cb0FdC | Participant   | https://www.delta-dao.com/.well-known/20.json  |
| Gaia-X Workshop 21 | 0x8BF36BEFC22a7b9c1a546139bFd4ae8420bcFf0e | Participant   | https://www.delta-dao.com/.well-known/21.json  |
| Gaia-X Workshop 22 | 0x2dB30B996C0E2990F836685Cf1A2939b3299f8e5 | Participant   | https://www.delta-dao.com/.well-known/22.json  |
| Gaia-X Workshop 23 | 0x224482ebcf914b9FA9E312036B377e26B676E534 | Participant   | https://www.delta-dao.com/.well-known/23.json  |
| Gaia-X Workshop 24 | 0xD580c01E2f503287006138a94eBBc537Fe7eBD25 | Participant   | https://www.delta-dao.com/.well-known/24.json  |
| Gaia-X Workshop 25 | 0x4B107057aB8278c7d9436bf76230d16e5F7BaD16 | Participant   | https://www.delta-dao.com/.well-known/25.json  |
| Gaia-X Workshop 26 | 0x7bf493b142AB0bb37c7f766A1585245901891685 | Participant   | https://www.delta-dao.com/.well-known/26.json  |
| Gaia-X Workshop 27 | 0x1c0c9211E8Ec8E0253A53880D5481e4580B62125 | Participant   | https://www.delta-dao.com/.well-known/27.json  |
| Gaia-X Workshop 28 | 0xa702032E187E6A53EAddC28a735B414220712689 | Participant   | https://www.delta-dao.com/.well-known/28.json  |
| Gaia-X Workshop 29 | 0xEEe803bEFd2B4f229E57523Edb11CDE38DD1a23E | Participant   | https://www.delta-dao.com/.well-known/29.json  |
| Gaia-X Workshop 30 | 0xb828bA1850aA11daA1890896573Aa6008221A671 | Participant   | https://www.delta-dao.com/.well-known/30.json  |
| Gaia-X Workshop 31 | 0x005d24698bF41c398ECF15a93455621932a6e19F | Participant   | https://www.delta-dao.com/.well-known/31.json  |
| Gaia-X Workshop 32 | 0xF0956a2D1fE80E58d3532926Ba232317D6E12007 | Participant   | https://www.delta-dao.com/.well-known/32.json  |
| Gaia-X Workshop 33 | 0x746d4715c24fc4d26D02A558ACF98dC717C68E1e | Participant   | https://www.delta-dao.com/.well-known/33.json  |
| Gaia-X Workshop 34 | 0x1F05b3A352C2FCdE0DBdA6A74717Dc79C09c4765 | Participant   | https://www.delta-dao.com/.well-known/34.json  |
| Gaia-X Workshop 35 | 0x1Bf21DCb771Aba18B1D23AA6D8a619C1AB1811a4 | Participant   | https://www.delta-dao.com/.well-known/35.json  |
| Gaia-X Workshop 36 | 0x04FEA446847c3539d35Cca0a74Cb82Da811BAfc3 | Participant   | https://www.delta-dao.com/.well-known/36.json  |
| Gaia-X Workshop 37 | 0x69bF63B2Bb6A93fc4ff434595A72a4ED313E5698 | Participant   | https://www.delta-dao.com/.well-known/37.json  |
| Gaia-X Workshop 38 | 0xEdfd506dd449Cd06c91f51Fe9DfE4e3E57B2F8f5 | Participant   | https://www.delta-dao.com/.well-known/38.json  |
| Gaia-X Workshop 39 | 0xb2AF8b92bFaC5299Cb6EDEf16150BFD1d4d26a93 | Participant   | https://www.delta-dao.com/.well-known/39.json  |
| Gaia-X Workshop 40 | 0x0763BfBcBfA0126b5A5509fB1185b7b6476BdAd8 | Participant   | https://www.delta-dao.com/.well-known/40.json  |
| Gaia-X Workshop 41 | 0x54d2946677CC16E06Efd6161A4abFA17fc98Afc3 | Participant   | https://www.delta-dao.com/.well-known/41.json  |
| Gaia-X Workshop 42 | 0x5880C2C30C922FE700fc079e1b6BBa7e9E7DE577 | Participant   | https://www.delta-dao.com/.well-known/42.json  |
| Gaia-X Workshop 43 | 0xc2350eA5913511A95c1aBED51de377A0b92846Be | Participant   | https://www.delta-dao.com/.well-known/43.json  |
| Gaia-X Workshop 44 | 0x0c85Cd08E6643Fa3E4B75268431d19CcFC99C916 | Participant   | https://www.delta-dao.com/.well-known/44.json  |
| Gaia-X Workshop 45 | 0x1153265057782e8C57292CA590E50acC36037204 | Participant   | https://www.delta-dao.com/.well-known/45.json  |
| Gaia-X Workshop 46 | 0xF211efa0C51559e6730db3Ba6FE1f1D46A68BE14 | Participant   | https://www.delta-dao.com/.well-known/46.json  |
| Gaia-X Workshop 47 | 0x1c99F7C29EE0e79CAAD8E4d0Cc0b95D5Ece62294 | Participant   | https://www.delta-dao.com/.well-known/47.json  |
| Gaia-X Workshop 48 | 0x7209bd8fDd841358a3CF9E7DaD8D9dCe2E4BbBB8 | Participant   | https://www.delta-dao.com/.well-known/48.json  |
| Gaia-X Workshop 49 | 0xDB5807EacA2937f6264c5725538f8Ec357b4d3b2 | Participant   | https://www.delta-dao.com/.well-known/49.json  |
| Gaia-X Workshop 50 | 0xB21282F443EB0D490819d98F2976758af5C979B3 | Participant   | https://www.delta-dao.com/.well-known/50.json  |